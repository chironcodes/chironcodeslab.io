Asdf is a well-known plugin to manage versions of node/python etc etc etc

### Installing:
```bash
# Downloads Asdf
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.11.1
```

### (Zsh) Add the following to `~/.zshrc`:
```bash
. $HOME/.asdf/asdf.sh

# append completions to fpath
fpath=(${ASDF_DIR}/completions $fpath)
# initialise completions with ZSH's compinit
autoload -Uz compinit && compinit
```

### (Bash) Add the following to `~/.bashrc`:
```bash
. $HOME/.asdf/asdf.sh
. $HOME/.asdf/completions/asdf.bash
```


.

