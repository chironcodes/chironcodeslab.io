#  Windows

Windows gets good as soon as you open your cmd as administrator and type the following code:

```bash
wsl --install
```


It'll get WSL up and running quickly. Probably it'll require a restart or two and then:

```wsl
code .
```

And you got an environment decent enough to get your code running.


.