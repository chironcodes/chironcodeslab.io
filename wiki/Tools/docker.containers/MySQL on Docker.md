# MySQL on Docker


- Creating your content
```bash
# Downloads MySQL
docker pull mysql[:version]

# Create a container with the downloaded image 
docker run --name mysql \
	-e MYSQL_ROOT_PASSWORD=mysql \
	-p 3306:3306 \
	-d mysql
```



- Accessing your environment
```bash
# Acessing container from terminal
docker exec -it mysql bash

# Acess interactive terminal inside your container
mysql -u root -p
```