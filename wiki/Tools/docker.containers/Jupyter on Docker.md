# Jupyter on Docker

- This Jupyter Notebook gives you access to a proper data engineering/scientist environment.

```bash
# Downloads Jupyter's image
docker pull jupyter/all-spark-notebook

# Create a container with the downloaded image 
docker run --name myjupyter \
	-v $(pwd):/home/jovyan/work \ 
	-p 4040:4040 \
	-p 8888:8888 \
	jupyter/all-spark-notebook

```



.