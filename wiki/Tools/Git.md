# Git

## Installing
- Git is installed by default in many linux distros. If you're unsure about, write the following command on your terminal.

```bash
git --version
>> git version 2.35.1
```




If your don't see a message like this, you must install it manually!

```bash
sudo apt-get install git -y
```



## Configuring your account
```bash

git config --global user.name "John Doe"
git config user.name

git config --global user.email "john@doe.com"
git config user.email

git config --global init.defaultBranch main
git config init.defaultBranch


```


## Daily flow
```bash
# Add all files from current folder
git add .

# Add a comment to your commit. The comment is available to all others.
git commit -m 'Write about your commit'

# Effectively 'upload' your changes to your remote repository
git push origin main

```


## Branches(forks)
 Branches or forks are ramification of a project and every project has at least one, called `main/master`. It's a common practice in a collaborative working environment to have many features in development state at the same time, and branching helps a lot with the flow of code
 
![[git_branching.png]]

## Branching
```bash
#Cria uma branch
git branch <nova_branch>

# Switch to the branch with recent modifications
git checkout <branch_name>

#Create a brand new branch and switchs to it
git checkout -b <branc_name>

# Deletes a branch LOCALLY
git branch -D <branch_name>

```



## Maneuvers(combo)!

## Wipe and fetch
```bash
# Clean all local changes
git clean -d -x -f

# Ignore all local change and get remote code
git fetch --all && git reset --hard origin/main

```

.