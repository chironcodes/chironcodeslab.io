# Cassandra on Docker


- Creating your content
```bash
# Downloads Cassandra's image
docker pull cassandra[:version]

# Create a container with the downloaded image 
docker run -d --name cassandra \
	-p 9042:9042 \
	-m=3g cassandra
```



- Accessing your environment
```bash
# Acessing container from terminal
docker exec -it cassandra bash

# Acess your cqlsh
cqlsh
```