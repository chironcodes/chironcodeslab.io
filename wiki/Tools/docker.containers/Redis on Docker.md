# Redis on Docker


- Creating your content
```bash
# Downloads Redis
docker pull redis[:version]

# Create a container with the downloaded image 
docker run --name redis \
	-p 6379:6379 \
	-d redis[3.2.5-alpine]
```



- Accessing your environment
```bash
# Acessing container from terminal
docker exec -it cassandra bash

# Acess interactive terminal inside your container
redis
```