# Hadoop

Its ecosystem is still on today's Big Data scene. Your distributed and high failure tolerance distributed file system **(HDFS)** provides high responsibility and low implementation cost, allowing us to implement a cheap and easy to set **Data Lake**.

Yarn is also a big actor in our Hadoop ecosystem. It runs in our main node and has the role to distribute source and tasks amongst others node. A good approach Yarn takes is to **designate tasks where or nearby data is** greatly reducing network overuse.



