# Linux

If we're talking about productivity, **Gnome** is ideal. It's everyone daily drive and for this reason it's much harder to break and if it breaks, you got a HUGE community by your side!

## Must have extensions:

- Sound Input & Output Device Choose
	https://extensions.gnome.org/extension/906/sound-output-device-chooser/

- Clipboard Indicator
	https://extensions.gnome.org/extension/779/clipboard-indicator/

- Caffeine
	https://extensions.gnome.org/extension/517/caffeine/






## A few good-to-know commands

```bash
# Get the path where your binary is located
which <package_name>


# Lists dependecies and some info related to your binary
dpkg -s <package_name>

# List files in known repos which contain the string
apt-cache search remmina

```


## Usefull programs
-  Remmina 
	Manage multiple SSH, VNC, RDP sessions

- 7zip
	zip/rar compatibility

- Xournal++
	Write and make notes with a drawing table. Multiplataform.

- Peek
	Create gifs from


.