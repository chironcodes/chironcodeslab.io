# Home

This is a repository of things that I found to be useful. It covers a wide variety of tech-related topics, and overall is what I use as a reference.


# Operational Systems
- [[ Windows ]]
- [[Linux]]


# Tools
- [[Git]] 
- [[Docker]]

