Mise-en-place is another option to manage versions of node/python/java  etc etc etc

### Installing:
```bash
# Downloads Mise-en-place
curl https://mise.run | sh
```

### (bashrc) Add the following to `~/.bashrc`:
```bash
echo 'eval "$(~/.local/bin/mise activate bash)"' >> ~/.bashrc
```

### (mise) Install python 3.11 as global:
```bash
mise use -g python@3.11
```


.