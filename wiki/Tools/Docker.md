#  Docker

![[docker_icon.png]]

- SQL DBS
	- [[MySQL on Docker]]
	- [[Postgres on Docker]]
	- [[Oracle on Docker]]


- NoSQL DB
	- [[Mongo on Docker]]
	- [[Redis on Docker]]
	- [[Cassandra on Docker]]


- ETL Tools
	- [[NiFi on Docker]]


- Data Visualization
	- [[Jupyter on Docker]]

.