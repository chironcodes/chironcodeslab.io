# Postgres on Docker


- Creating your content
```bash
# Downloads PostgreSQL
docker pull postgres[:version]

# Create a container with the downloaded image 
docker run --name postgres \
	-e POSTGRES_PASSWORD=postgres \
	-p 5432:5432 \
	-d  postgres
```



- Accessing your environment
```bash
# Acessing container from terminal
docker exec -it postgres bash

# Acess interactive terminal inside your container
psql -U postgres
```