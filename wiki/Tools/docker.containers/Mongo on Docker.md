# Mongo on Docker


- Creating your content
```bash
# Downloads MongoDB
docker pull mongo[:version]

# Create a container with the downloaded image 
docker run --name mongodb \
	-e MONGO_INITDB_ROOT_USERNAME=mongodb \
	-e MONGO_INITDB_ROOT_PASSWORD=mongodb \
	-p 27017:27017 \
	-d mongo
```



- Accessing your environment
```bash
# Acessing container from terminal
docker exec -it mongodb bash

# Acess interactive terminal from mongo
mongosh
mongo
```