# Python on Asdf


- Install the plugin with this command
```bash
asdf plugin-add python
```


- List all avaliable versions of a plugin
```bash
asdf list all python
```


- Install requirements
ref: https://github.com/pyenv/pyenv/wiki#suggested-build-environment
```bash
sudo apt-get install bzip2 libreadline6-dev openssl \
  build-essential libssl-dev \
  zlib1g-dev libbz2-dev libreadline-dev \
  libsqlite3-dev llvm libncurses5-dev liblzma-dev\
  libncursesw5-dev wget curl python-tk python3-tk tk-dev
```

```bash
sudo dnf install make gcc patch zlib-devel \
 bzip2 bzip2-devel readline-devel sqlite sqlite-devel \
 openssl-devel tk-devel libffi-devel xz-devel \
 libuuid-devel gdbm-libs libnsl2

```


- Install the latest version
```bash
asdf install python 3.11.13
```


- Set a version as global
```bash
asdf global python 3.8.4
```