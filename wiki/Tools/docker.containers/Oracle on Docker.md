# Oracle on Docker


- Creating your content
```bash
# Downloads Oracle's image
docker pull store/oracle/database-enterprise:12.2.0.1[-slim]


# Create a container with the downloaded image 
docker run --name oracle 
	-p 1521:1521  
	-d store/oracle/database-enterprise:12.2.0.1
```



- Accessing your environment
```bash
# Acessing container from terminal
docker exec -it oracle bash

# Acess your oracle
??
```