# Vscode on Docker


- Creating your content
```bash
# Downloads Code-servers's image
docker pull linuxserver/code-server[:version]

# Create a container with the downloaded image 
docker run \
    --name code-server \
    -p 8443:8443 \
    -v $(pwd):/config/workspace \
    -e PUID=$(id -u) \
    -e PGID=$(id -g) \
    linuxserver/code-server:latest

-e PUID/GID sets make files created within container acessible and editable from host
```

