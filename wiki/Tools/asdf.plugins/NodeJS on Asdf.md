# NodeJS on Asdf 


- Install the plugin with this command
```bash
asdf plugin-add nodejs
```


- List all avaliable versions of a plugin
```bash
asdf list all nodejs
```


- Install the latest version
```bash
asdf install nodejs 14.17.6
```


- Set a version as global
```bash
asdf global nodejs 14.17.6
#asdf install nodejs 12.13.0 required for vagrant
```